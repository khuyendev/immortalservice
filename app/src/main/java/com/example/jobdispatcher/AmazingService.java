package com.example.jobdispatcher;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.widget.Toast;

public class AmazingService extends Service {
    public AmazingService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Toast.makeText(getApplicationContext(), "AmazingService Wakeup", Toast.LENGTH_LONG).show();
    }
    
}
