package com.example.jobdispatcher;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.internal.view.SupportMenu;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.firebase.jobdispatcher.Constraint;
import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;
import com.firebase.jobdispatcher.Job;
import com.firebase.jobdispatcher.Lifetime;
import com.firebase.jobdispatcher.RetryStrategy;
import com.firebase.jobdispatcher.Trigger;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
	private static final String JOB_TAG = "MyJobService";
	private FirebaseJobDispatcher mDispatcher;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		findViewById(R.id.btn_schedule).setOnClickListener(this);
		findViewById(R.id.btn_cancel).setOnClickListener(this);

		mDispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(getApplicationContext()));
		registerNextAlarmNotification();
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
			case R.id.btn_schedule: scheduleJob(); break;
			case R.id.btn_cancel: cancelJob(JOB_TAG); break;
		}
	}

	private void scheduleJob() {
		Job myJob = mDispatcher.newJobBuilder()
				.setService(MyJobService.class)
				.setTag(JOB_TAG)
				.setRecurring(true)
				.setTrigger(Trigger.executionWindow(5, 7))
				.setLifetime(Lifetime.UNTIL_NEXT_BOOT)
				.setReplaceCurrent(false)
				.setConstraints(Constraint.ON_ANY_NETWORK)
				.setRetryStrategy(RetryStrategy.DEFAULT_EXPONENTIAL)
				.build();
		
		mDispatcher.mustSchedule(myJob);
		Toast.makeText(this, R.string.job_scheduled, Toast.LENGTH_LONG).show();
	}


	private void cancelJob(String jobTag) {
		if ("".equals(jobTag)) {
			mDispatcher.cancelAll();
		} else {
			mDispatcher.cancel(jobTag);
		}
		Toast.makeText(this, R.string.job_cancelled, Toast.LENGTH_LONG).show();
	}
	private  void registerNextAlarmNotification() {
		PendingIntent activity = PendingIntent.getActivity(getApplicationContext(), 0, new Intent(getApplicationContext(), MainActivity.class), 0);
		String str = "com.malangstudio.alarmmon:next alarm";
		CharSequence string = "ahihi";
		if (Build.VERSION.SDK_INT >= 26) {
			NotificationChannel notificationChannel = new NotificationChannel(str, string, 2);
			notificationChannel.enableLights(false);
			notificationChannel.setLightColor(SupportMenu.CATEGORY_MASK);
			notificationChannel.setShowBadge(false);
			notificationChannel.setLockscreenVisibility(1);
			((NotificationManager) getApplicationContext().getSystemService("notification")).createNotificationChannel(notificationChannel);
		}
		((NotificationManager) getApplicationContext().getSystemService("notification"))
				.notify(3, new NotificationCompat.Builder(this, str).setOngoing(true)
						.setContentTitle("hihi").setStyle(new NotificationCompat.BigTextStyle().bigText("hihi")).setContentText("hihi")
						.setPriority(2).setContentIntent(activity).setSmallIcon(android.R.drawable.ic_dialog_alert).build());
	}
}